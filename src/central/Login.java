package central;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JPasswordField;

public class Login extends JFrame {

	private JPanel contentPane;
	private JTextField usrName;
	private JPasswordField usrPass;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login frame = new Login();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Login() {
		setResizable(false);
		setTitle("Login");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 344, 233);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.setBounds(10, 11, 318, 182);
		contentPane.add(panel);
		panel.setLayout(null);
		
		usrName = new JTextField();
		usrName.setBounds(147, 31, 86, 20);
		panel.add(usrName);
		usrName.setColumns(10);
		
		usrPass = new JPasswordField();
		usrPass.setBounds(147, 78, 86, 20);
		panel.add(usrPass);
		
		JLabel lblEnterUsername = new JLabel("Enter Username");
		lblEnterUsername.setFont(new Font("Arial", Font.PLAIN, 11));
		lblEnterUsername.setBounds(47, 34, 90, 14);
		panel.add(lblEnterUsername);
		
		JLabel lblEnterPassword = new JLabel("Enter Password");
		lblEnterPassword.setBounds(47, 81, 90, 14);
		panel.add(lblEnterPassword);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.setBounds(109, 125, 89, 23);
		panel.add(btnLogin);
	}
}
